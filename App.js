import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {FlatList} from "react-native-web";

export default class App extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      data:[]
    }
  }
  componentDidMount = async () =>{
    return fetch('http://18.198.15.69:8000/age/', {
        method: 'post',
        body: JSON.stringify({age:40})
    })
        .then(res => {return res.json()})
        .then(response => {
          this.setState({data:[response]})
            console.log(response)
        })
        .catch(err=>{
          console.error(err)
        })
  }


  render() {
    return (
        <View style={styles.container}>
          <Text>Open up App.js to start working on </Text>
          <FlatList data = {this.state.data}
                    renderItem={({ item }) => (
                        <Text>{item.nouveau_age}</Text>
                        )}
          />
          <StatusBar style="auto" />
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
